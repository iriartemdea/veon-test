package br.com.iriarte.service

import br.com.iriarte.api._
import br.com.iriarte.model.{DBAccess, MovieDB, ReservationDB}
import com.twitter.util.{Await, Future}
import org.mockito.Matchers
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.scalatest.FlatSpec
import org.scalatest.mockito.MockitoSugar

class MovieServiceSpec extends FlatSpec with MockitoSugar {

  var testDupe = false

  val validIMDB = common.IMDBId("validmovie")
  val validScreen = common.ScreenId("validScreen")
  val invalidIMDB = common.IMDBId("---")

  val fullvalidIMDB = common.IMDBId("fullmovie")

  val movie = Movie(validIMDB, Reservation.AvailableSeats(10), validScreen)
  val movieDB = MovieDB(1, validIMDB.value, 10, validScreen.value)
  val fullMovieDB = MovieDB(1, fullvalidIMDB.value, 5, validScreen.value)
  val movieInfo = MovieInfo.create(validIMDB.value, movieDB.availableSeats, 5, validScreen.value, "")

  val validReservation = Reservation(validIMDB, validScreen)
  val validReservationDB = ReservationDB(1, 1)
  val dbaccess = mock[DBAccess]
  when(
    dbaccess.findMovie(
      Matchers.eq(validIMDB.value),
      Matchers.eq(validScreen.value))).thenAnswer(new Answer[Future[Option[MovieDB]]] {
    override def answer(invocation: InvocationOnMock) = {
      if (testDupe)
        Future(None)
      else Future(Option(movieDB))
    }
  })
  when(
    dbaccess.findMovie(
      Matchers.eq(fullvalidIMDB.value),
      Matchers.eq(validScreen.value))).
    thenReturn(Future(Option(fullMovieDB)))
  when(dbaccess.findMovie(Matchers.eq(invalidIMDB.value), Matchers.any())).thenReturn(Future(None))
  when(dbaccess.countReservations(Matchers.any(), Matchers.any())).thenReturn(Future(5L))
  when(dbaccess.insertReservation(Matchers.any(), Matchers.any())).thenReturn(Future(Option(validReservationDB)))


  when(dbaccess.insertMovie(Matchers.any(), Matchers.any(), Matchers.any())).thenAnswer(new Answer[Future[Option[MovieDB]]] {
    override def answer(invocation: InvocationOnMock) = {
      if (testDupe) {
        testDupe = false
        Future(Option(movieDB))
      } else {
        testDupe = true
        Future(None)
      }
    }
  })

  val service = new MovieServiceImpl(dbaccess)

  "A valid pair imdb-screen id" should "return a Movie" in {
    val res = Await.result(service.findMovieInfo(validIMDB, validScreen))
    assert(res == Option(movieInfo))
  }
  "A INVALID pair imdb-screen id" should "return a None" in {
    val res = Await.result(service.findMovieInfo(invalidIMDB, validScreen))
    assert(res.isEmpty)
  }

  "A reservation on an available movie" should "return a Reservation" in {
    val res = Await.result(service.insertReservation(validIMDB, validScreen))
    assert(res == Right(validReservation))
  }
  "A reservation on an full movie" should "NOT be possible" in {
    val res = Await.result(service.insertReservation(fullvalidIMDB, validScreen))
    assert(res.isLeft)
  }

  "A missing pair movie-screen" should "return the Movie" in {
    testDupe = true
    val res = Await.result(service.insertMovie(movie))
    assert(res == Option(movie))
  }

  "Insert twice a pair movie-screen" should "not be possible to insert" in {
    testDupe = true
    val res = Await.result(service.insertMovie(movie))
    assert(res == Option(movie))
    val res2 = Await.result(service.insertMovie(movie))
    assert(res2.isEmpty)
  }
}
