package br.com.iriarte.service


import br.com.iriarte.api.common
import com.twitter.util.Await
import org.scalatest._

class TMDBServiceImplSpec extends FlatSpec {
  val imdbService = new TMDBServiceImpl
  val validImdb = common.IMDBId("tt4117094")
  val invalidImdb = common.IMDBId("MORE-THAN-INVALID")
  "A valid IMDB Id" should "return a valid movie title" in {
    val result = Await.result(imdbService.findMovieName(validImdb))
    assert(result.isDefined)
  }
  "An INVALID IMDB Id" should "return an empty title" in{
    val result = Await.result(imdbService.findMovieName(invalidImdb))
    assert(result.isEmpty)
  }
}
