package br.com.iriarte.api

import br.com.iriarte.api.Movie.Title
import br.com.iriarte.api.Reservation._
import br.com.iriarte.api.common._
import br.com.iriarte.service.{ImdbService, MovieService}
import com.twitter.finagle.http.Status
import com.twitter.util.Future
import io.circe.generic.auto._
import io.finch.circe._
import io.finch.{Application, Input}
import org.mockito.Mockito._
import org.mockito.{Matchers => MockitoMatchers}
import org.scalatest._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.prop.Checkers

class ApiSpec extends FlatSpec with MockitoSugar with Matchers with Checkers with CirceEncoders{


  lazy val apiDefinition = new ApiDefinition(mockService, mockIMDB)
  val validIMDBId = IMDBId("validIMDB")
  val validIMDBIdMatcher = () => IMDBId(MockitoMatchers.eq("validIMDB"))
  val availableSeats = AvailableSeats(10)
  val reservedSeats = ReservedSeats(4)
  val validScreenId = ScreenId("screen")
  val validScreenIdMatcher = () => ScreenId(MockitoMatchers.eq("screen"))
  val validMovie = Movie(validIMDBId, availableSeats, validScreenId)
  val validReservation = Reservation(validIMDBId, validScreenId)
  val validTitle = Title("validtitle")
  val dbMovieInfo= MovieInfo(validIMDBId, availableSeats, reservedSeats, validScreenId, Title("title"))
  val validMovieInfo = MovieInfo(validIMDBId, availableSeats, reservedSeats, validScreenId, validTitle)

  val unknownIMDBId = IMDBId("unknown")
  val unknownIMDBIdMatcher = () => IMDBId(MockitoMatchers.eq("unknown"))
  val unknowTitle = Title("unavailable")
  val unknownMovieInfo = MovieInfo(unknownIMDBId, availableSeats, reservedSeats, validScreenId, unknowTitle)

  val invalidIMDBId = IMDBId("INVALID")
  val invalidIMDBIdMatcher = () => IMDBId(MockitoMatchers.eq("INVALID"))
  val invalidMovie = Movie(invalidIMDBId, availableSeats, validScreenId)

  val noScreen = ScreenId("NOSCREEN")
  val noScreenMatcher = () => ScreenId(MockitoMatchers.eq("NOSCREEN"))
  val noScreenReservation = Reservation(validIMDBId, noScreen)

  val fullScreen = ScreenId("Full")
  val fullScreenMatcher = () => ScreenId(MockitoMatchers.eq("Full"))
  val fullReservation = Reservation(validIMDBId, fullScreen)

  lazy val mockService = mock[MovieService]
  when(mockService.insertMovie(MockitoMatchers.eq(validMovie))).thenReturn(Future(Some(validMovie)))
  when(mockService.insertMovie(MockitoMatchers.eq(invalidMovie))).thenReturn(Future(None))

  when(mockService.insertReservation(validIMDBIdMatcher(), fullScreenMatcher())).thenReturn(Future(Left("No available seats")))
  when(mockService.insertReservation(validIMDBIdMatcher(), noScreenMatcher())).thenReturn(Future(Left("Movie and screen not found")))
  when(mockService.insertReservation(validIMDBIdMatcher(), validScreenIdMatcher())).thenReturn(Future(Right(validReservation)))

  when(mockService.findMovieInfo(validIMDBIdMatcher(), validScreenIdMatcher())).thenReturn(Future(Some(dbMovieInfo)))
  when(mockService.findMovieInfo(invalidIMDBIdMatcher(), validScreenIdMatcher())).thenReturn(Future(None))
  when(mockService.findMovieInfo(unknownIMDBIdMatcher(), validScreenIdMatcher())).thenReturn(Future(Some(unknownMovieInfo)))
  lazy val mockIMDB = mock[ImdbService]
  when(mockIMDB.findMovieName(IMDBId(MockitoMatchers.eq(validIMDBId.value)))).thenReturn(Future(Some(validTitle)))
  when(mockIMDB.findMovieName(IMDBId(MockitoMatchers.eq(unknownIMDBId.value)))).thenReturn(Future(None))

  "Register movie" should "return 200 on valid arguments" in {
    val endpoint = apiDefinition.registerMovie
    val input = Input.post("/movie").withBody[Application.Json](validMovie)
    val Some(output) = endpoint(input).awaitOutputUnsafe()
    output.status shouldBe Status.Ok
  }

  "Register movie" should "return 400 on invalid arguments" in {
    val endpoint = apiDefinition.registerMovie
    val input = Input.post("/movie").withBody[Application.Json](invalidMovie)
    val Some(output) = endpoint(input).awaitOutputUnsafe()
    output.status shouldBe Status.BadRequest
  }

  "Reserve a valid seat" should "be possible" in {
    val endpoint = apiDefinition.reserveSeat
    val input = Input.post("/movie/reservation").withBody[Application.Json](validReservation)
    val Some(output) = endpoint(input).awaitOutputUnsafe()
    output.status shouldBe Status.Ok
  }

  "Reserve a seat in a full screen" should " NOT be possible" in {
    val endpoint = apiDefinition.reserveSeat
    val input = Input.post("/movie/reservation").withBody[Application.Json](fullReservation)
    val exc = the[Exception] thrownBy {
      endpoint(input).awaitValueUnsafe()
    }
    exc.getMessage should startWith("No available seats")
  }

  "Reserve a seat in a NO SCREEN" should " NOT be possible" in {
    val endpoint = apiDefinition.reserveSeat
    val input = Input.post("/movie/reservation").withBody[Application.Json](noScreenReservation)
    val exc = the[Exception] thrownBy {
      endpoint(input).awaitValueUnsafe()
    }
    exc.getMessage should startWith("Movie and screen not found")
  }

  "A valid movie info" should "be possible" in {
    val endpoint = apiDefinition.screeningInfo
    val input = Input.get(s"/movie/${validIMDBId.value}/screen/${validScreenId.value}")
    val Some(output) = endpoint(input).awaitOutputUnsafe()
    output.status shouldBe Status.Ok
    output.value shouldBe validMovieInfo
  }

  "A valid movie info but a fail on imdb service" should "be possible" in {
    val endpoint = apiDefinition.screeningInfo
    val input = Input.get(s"/movie/${unknownIMDBId.value}/screen/${validScreenId.value}")
    val Some(output) = endpoint(input).awaitOutputUnsafe()
    output.status shouldBe Status.Ok
    output.value shouldBe unknownMovieInfo
  }

  "An invalid movie info " should "NOT be possible" in {
    val endpoint = apiDefinition.screeningInfo
    val input = Input.get(s"/movie/${invalidIMDBId.value}/screen/${validScreenId.value}")
    val Some(output) = endpoint(input).awaitOutputUnsafe()
    val exc = the[Exception] thrownBy {
      endpoint(input).awaitValueUnsafe()
    }
    exc.getMessage should startWith("Could not find")
  }
}
