package br.com.iriarte.service

import java.net.URL

import br.com.iriarte.api.Movie.Title
import br.com.iriarte.api.{Movie, common}
import com.twitter.util.Future
import com.typesafe.config.ConfigFactory
import featherbed.circe._
import featherbed.request.ErrorResponse
import io.circe.generic.auto._
import shapeless.Coproduct

trait ImdbService {
  def findMovieName(imdbId: common.IMDBId): Future[Option[Movie.Title]]
}

case class TMDBMovieResponse(title: String)

class TMDBServiceImpl extends ImdbService {

  lazy val config = ConfigFactory.load()
  lazy val base = config.getString("tmdb.base")
  lazy val endpoint = config.getString("tmdb.url")
  lazy val token = config.getString("tmdb.apikey")
  lazy val client = new featherbed.Client(new URL(base))

  override def findMovieName(imdbId: common.IMDBId) = {
    client.get(s"$endpoint/${imdbId.value}?api_key=$token").accept[Coproduct.`"application/json"`.T].send[TMDBMovieResponse].
      map(i => Option(Title(i.title))).
      handle{ case ErrorResponse(_,_ )=> None}
  }
}
