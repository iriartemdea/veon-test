package br.com.iriarte.service

import br.com.iriarte.api.{Movie, MovieInfo, Reservation, common}
import br.com.iriarte.model.DBAccess
import cats.data.OptionT
import com.twitter.util.Future
import io.catbird.util._

trait MovieService {
  def findMovieInfo(imdbId: common.IMDBId, screenId: common.ScreenId): Future[Option[MovieInfo]]

  def insertReservation(imdbId: common.IMDBId, screenId: common.ScreenId): Future[Either[String, Reservation]]

  def insertMovie(movie: Movie): Future[Option[Movie]]
}

class MovieServiceImpl(dBAccess: DBAccess) extends MovieService {


  override def findMovieInfo(imdbId: common.IMDBId, screenId: common.ScreenId) = {
    for {
      movie <- dBAccess.findMovie(imdbId.value, screenId.value)
      count <- dBAccess.countReservations(imdbId.value, screenId.value)
    } yield {
      movie.map(m => {
        MovieInfo.create(m.imdbId, m.availableSeats, count.toInt, m.screenId, "")
      })
    }
  }

  override def insertReservation(imdbId: common.IMDBId, screenId: common.ScreenId) = {
    findMovieInfo(imdbId, screenId).flatMap {
      case Some(movieInfo) if movieInfo.hasAvailableSeats => {
        dBAccess.insertReservation(imdbId.value, screenId.value).map {
          case Some(reservation) => Right(Reservation(imdbId, screenId))
          case None => Left("Could not reserve")
        }
      }
      case Some(movieInfo) => {
        Future(Left("No available seats"))
      }
      case None => {
        Future(Left("Movie and screen not found"))
      }
    }
  }

  private def invertOption[T](opt:Option[T]):Option[Unit]={
    opt match {
      case Some(_) => None
      case None => Some(())
    }
  }

  override def insertMovie(movie: Movie) = {
    (for {
      _ <- OptionT(dBAccess.findMovie(movie.imdbId.value, movie.screenId.value).map(invertOption _))
      _ <- OptionT(dBAccess.insertMovie(movie.imdbId.value, movie.screenId.value, movie.availableSeats.value))
    } yield {
      movie
    }).value
  }
}
