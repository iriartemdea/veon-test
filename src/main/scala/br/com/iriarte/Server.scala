package br.com.iriarte

import com.twitter.app.Flag
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finagle.{Http, Service}
import com.twitter.server.TwitterServer
import com.twitter.util.Await
import io.circe.{Encoder, Json}

object Server extends TwitterServer with ServiceModule {
  def errorToJson(e: Exception): Json = Json.obj("error" -> Json.fromString(e.getMessage))


  implicit val ee: Encoder[Exception] = Encoder.instance {
    case e => errorToJson(e)
  }

  val port: Flag[Int] = flag("port", 8081, "TCP port for HTTP server")

  val api: Service[Request, Response] = (
    definition.api
    )


  def main(): Unit = {
    log.info("Initializing server")

    val server = Http.server
      .withAdmissionControl
      .concurrencyLimit(
        maxConcurrentRequests = 10,
        maxWaiters = 100)
      .serve(s":${port()}", api)
    onExit {
      server.close()
    }

    Await.ready(server)
  }
}
