package br.com.iriarte.model

import com.twitter.util.Future
import io.getquill._

class DBAccess {


  lazy val ctx = new H2JdbcContext[Literal]("ctx")

  import ctx._

  val movieQuery = quote {
    query[MovieDB]
  }
  val filteredMovieQuery = (imdbid: String, screenId: String) => quote {
    movieQuery.filter(movie => movie.imdbId == lift(imdbid) && movie.screenId == lift(screenId))
  }
  val reservationQuery = quote {
    query[ReservationDB]
  }

  def findMovie(imdbId: String, screenId: String): Future[Option[MovieDB]] = {
    val q = quote {
      filteredMovieQuery(imdbId, screenId)
    }
    Future(ctx.run(q).headOption)
  }

  def countReservations(imdbId: String, screenId: String): Future[Long] = {
    lazy val q = quote {
      (for {
        movie <- filteredMovieQuery(imdbId, screenId)
        reservation <- reservationQuery if movie.movieId == reservation.movieId
      } yield reservation).size
    }
    Future(ctx.run(q))
  }

  def insertReservation(imdbId: String, screenId: String): Future[Option[ReservationDB]] = {
    lazy val movie = ctx.run(filteredMovieQuery(imdbId, screenId)).head
    lazy val q = quote(reservationQuery.insert(_.movieId -> lift(movie.movieId)).returning(_.reservationId))
    Future(ctx.run(q)).map(l => Option(ReservationDB(l, movie.movieId)))
  }
  def insertMovie(imdbId: String, screenId: String, availableSeats: Int) = {
    lazy val q  = quote {
      movieQuery.insert(
        _.imdbId -> lift(imdbId),
        _.availableSeats -> lift(availableSeats) ,
        _.screenId -> lift(screenId)).returning(_.movieId)
    }
    Future(ctx.run(q)).map(l => Option(MovieDB(l, imdbId, availableSeats, screenId)))
  }

}
