package br.com.iriarte.model

case class ReservationDB(reservationId: Long, movieId: Long)

case class MovieDB(movieId: Long, imdbId: String, availableSeats: Int, screenId: String)
