package br.com.iriarte.api

abstract class ServerError(msg: String) extends Exception(msg) {
  def message: String
}

case class InvalidInput(message: String) extends ServerError(message)

case class RecordNotFound(message: String) extends ServerError(message)
