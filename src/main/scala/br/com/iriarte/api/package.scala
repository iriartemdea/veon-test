package br.com.iriarte

import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._
import cats.syntax.either._

package object api {

  object common {

    case class IMDBId(value: String) extends AnyVal

    case class ScreenId(value: String) extends AnyVal


  }

  case class Movie(
                    imdbId: common.IMDBId,
                    availableSeats: Reservation.AvailableSeats,
                    screenId: common.ScreenId)

  object Movie {

    case class Title(value: String) extends AnyVal

  }

  case class Reservation(
                          imdbId: common.IMDBId,
                          screenId: common.ScreenId)

  object Reservation {

    case class AvailableSeats(value: Int) extends AnyVal

    case class ReservedSeats(value: Int) extends AnyVal


  }

  case class MovieInfo(
                        imdbId: common.IMDBId,
                        availableSeats: Reservation.AvailableSeats,
                        reservedSeats: Reservation.ReservedSeats,
                        screenId: common.ScreenId,
                        movieTitle: Movie.Title) {
    def hasAvailableSeats: Boolean = (availableSeats.value - reservedSeats.value) > 0


  }

  object MovieInfo {
    def create(
                imdbId: String,
                availableSeats: Int,
                reservedSeats: Int,
                screenId: String,
                movieTitle: String): MovieInfo =
      new MovieInfo(
        common.IMDBId(imdbId),
        Reservation.AvailableSeats(availableSeats),
        Reservation.ReservedSeats(reservedSeats),
        common.ScreenId(screenId), Movie.Title(movieTitle))
  }

}
