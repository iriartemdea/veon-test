package br.com.iriarte.api

import br.com.iriarte.api.Movie._
import br.com.iriarte.api.Reservation._
import br.com.iriarte.api.common._
import cats.syntax.either._
import io.circe.{Decoder, Encoder}

trait CirceEncoders {
  implicit val decodeIMDBId: Decoder[IMDBId] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(IMDBId(str)).leftMap(t => "-1")
  }
  implicit val decodeScreenId: Decoder[ScreenId] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(ScreenId(str)).leftMap(t => "-1")
  }
  implicit val encodeIMDBId: Encoder[IMDBId] = Encoder.encodeString.contramap[IMDBId](_.value)
  implicit val encodeScreenId: Encoder[ScreenId] = Encoder.encodeString.contramap[ScreenId](_.value)
  implicit val decodeAvailableSeats: Decoder[AvailableSeats] = Decoder.decodeInt.emap { v =>
    Either.catchNonFatal(AvailableSeats(v)).leftMap(t => "-1")
  }

  implicit val decodeReservedSeats: Decoder[ReservedSeats] = Decoder.decodeInt.emap { v =>
    Either.catchNonFatal(ReservedSeats(v)).leftMap(t => "-1")
  }
  implicit val encodeAvailableSeats: Encoder[AvailableSeats] = Encoder.encodeInt.contramap[AvailableSeats](_.value)
  implicit val encodeReservedSeats: Encoder[ReservedSeats] = Encoder.encodeInt.contramap[ReservedSeats](_.value)
  implicit val decodeTitle: Decoder[Title] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(Title(str)).leftMap(t => "-1")
  }
  implicit val encodeTitle: Encoder[Title] = Encoder.encodeString.contramap[Title](_.value)
}

object CirceEncoders extends CirceEncoders
