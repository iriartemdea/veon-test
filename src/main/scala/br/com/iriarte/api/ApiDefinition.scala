package br.com.iriarte.api

import br.com.iriarte.api.Movie._
import br.com.iriarte.api.common._
import br.com.iriarte.service.{ImdbService, MovieService}
import com.twitter.util.Future
import io.circe.generic.auto._
import io.circe.{Encoder, Json}
import io.finch._
import io.finch.circe._

class ApiDefinition(service: MovieService, imdbService: ImdbService) extends CirceEncoders{

  def jsonMovie: Endpoint[Movie] = jsonBody[Movie]

  def jsonReservation: Endpoint[Reservation] = jsonBody[Reservation]

  def beEmpty: ValidationRule[String] = ValidationRule[String]("be empty") {
    _.isEmpty
  }


  def registerMovie: Endpoint[Movie] = post("movie" :: jsonMovie) { newMovie: Movie =>
    service.insertMovie(newMovie).map {
      case Some(_) => Ok(newMovie)
      case None => BadRequest(new Exception(s"Duplicated movie-screen $newMovie"))
    }
  }

  def reserveSeat: Endpoint[Reservation] = post("movie" :: "reservation" :: jsonReservation) { newReservation: Reservation =>
    service.insertReservation(newReservation.imdbId, newReservation.screenId).map {
      case Right(reservation) => Ok(reservation)
      case Left(message) => BadRequest(new Exception(s"$message: $newReservation"))
    }
  }

  def screeningInfo: Endpoint[MovieInfo] = get("movie" :: string :: "screen" :: string) { (imdbId: String, screenId: String) =>
    service.findMovieInfo(IMDBId(imdbId), ScreenId(screenId)).flatMap {
      case Some(movieInfo) => imdbService.findMovieName(IMDBId(imdbId)).map {
        case Some(titleRet) => Ok(movieInfo.copy(movieTitle = titleRet))
        case None => Ok(movieInfo.copy(movieTitle = Title("unavailable")))
      }
      case None => Future(BadRequest(new Exception(s"Could not find [imdbid = $imdbId, screenid = $screenId] ")))
    }
  }


  def api = (registerMovie :+: reserveSeat :+: screeningInfo).toServiceAs[Application.Json]
}
