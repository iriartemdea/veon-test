package br.com.iriarte

import br.com.iriarte.api.ApiDefinition
import br.com.iriarte.model.DBAccess
import br.com.iriarte.service.{MovieService, MovieServiceImpl, TMDBServiceImpl}
import com.softwaremill.macwire.wire

trait ServiceModule {
  lazy val dbProvider = wire[DBAccess]
  lazy val movieService = wire[MovieServiceImpl]
  lazy val imdbService = wire[TMDBServiceImpl]
  lazy val definition = wire[ApiDefinition]
}
