organization := "br.com.iriarte"
name := "Movie Ticket"

version := "1.0"

scalaVersion := "2.11.11"

resolvers ++= Seq(
  Resolver.sonatypeRepo("snapshots"),
  Resolver.sonatypeRepo("releases")
)

libraryDependencies ++= Seq(
  "com.github.finagle" %% "finch-core" % "0.14.0",
  "com.github.finagle" %% "finch-circe" % "0.14.0",
  "io.github.finagle" %% "featherbed" % "0.3.0",
  "io.catbird" %% "catbird-util" % "0.14.0",
  "io.circe" %% "circe-generic" % "0.8.0",
  "com.typesafe" % "config" % "1.3.1",
  "com.twitter" %% "twitter-server" % "1.29.0",
  "com.h2database" % "h2" % "1.4.192",
  "io.getquill" %% "quill-jdbc" % "1.2.1",
  "com.twitter" %% "bijection-core" % "0.9.5",
  "com.twitter" %% "bijection-util" % "0.9.5",
  "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided",
  "com.softwaremill.macwire" %% "proxy" % "2.3.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.mockito" % "mockito-all" % "1.10.19" % "test"

)
